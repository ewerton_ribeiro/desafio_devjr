<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<style>
	body {
		background-color: lightblue;
		text-align: center;
		
		
		}
	h1{
		text-align: center;
		}

		
	form{
		margin: 20px 0;
		align: rigth;
		
		}
		
	form label, form legend{
		display: inline-block;
		font-size: 13px;
		margin: 0 5px 10px;
			
		
		}
	
	.input-Nome {
		display: inline-block;
		font-size: 13px;
		margin: 0 0 20px 0px;
		padding: 5px 25px;
		width: 120px;
		
		}
		
	.input-Cpf {
		display: inline-block;
		font-size: 13px;
		margin: 0 0 20px 45px;
		padding: 5px 25px;
		width: 100px;
		
		}
		
	.input-Salario {
		display: inline-block;
		font-size: 13px;
		margin: 0 20px 20px 25px;
		padding: 5px 25px;
		width: 70px;
		
		}
		
	.button-SignOf {
		position: absolute;
		top: 29px;
		right: 20px;
				
		}
	</style>
	
<html>
	<head>
	<meta charset="UTF-8">
	<title>Cadastro de Proposta</title>
	</head>
<%
		String login = (String) session.getAttribute("login");
		if(login == null){
			response.sendRedirect("Login.jsp?command=null");
		}
	%>

<body>
	<div class="a">
	<h1>Cadastro de Proposta</h1>
	
	<form action="cadastroProposta" method="post">
		<label for="nome"> Nome do Cliente: </label>
		<input class="input-Nome" type="text" name="nome" size="18" maxlength="45"/>
		<br/>
		
		<label for="CPF"> CPF: </label>
		<input class="input-Cpf" type="text" name="cpf" size="18" maxlength="14"/>
		<br/>
		
		<label for="nome"> Salario: </label>
		<input class="input-Salario" type="text" name="salario" size="18" maxlength="11"/>
		<br/>
		
		<input type="submit" value="Cadastrar Proposta"/><br/>
	</form>
		
		<a href="propostasAnalisadas"><button>Consultar Propostas Analisadas</button></a>
		<a class="button-SignOf" href="Controller?command=Deslogar"><button>SingOf</button></a>
	</div>
</body>
</html>