package command;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PerfilCaptacao implements Command {
	public void exe(HttpServletRequest request, HttpServletResponse response) {
		try {
			RequestDispatcher d = request.getRequestDispatcher("/PerfilCaptacao.jsp");
			d.forward(request, response);
		} catch(IOException | ServletException e) {
			e.printStackTrace();
		}
	}
}