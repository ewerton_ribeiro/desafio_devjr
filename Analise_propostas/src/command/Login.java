package command;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Login implements Command {
	public void exe(HttpServletRequest request, HttpServletResponse response) {
		try {
			RequestDispatcher d = request.getRequestDispatcher("/Login.jsp");
			d.forward(request, response);
		} catch(IOException | ServletException e) {
			e.printStackTrace();
		}
	}
}