package command;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ewe.com.elr.DAO.UsuarioDAO;

public class checkLogin extends HttpServlet implements Command {

	public void exe(HttpServletRequest request, HttpServletResponse response) {
		try {
			String login = request.getParameter("login");
			String senha = request.getParameter("senha");
			String tipo = "";
			
			if(login != null && senha != null && !login.isEmpty() && !senha.isEmpty()) {
				if(UsuarioDAO.checkLogin(login, senha) == true) {
					HttpSession session = request.getSession();
					session.setAttribute("login", login);
					tipo = UsuarioDAO.checkTipo(login);
					if(tipo.equals("captacao")) {
						response.sendRedirect("Controller?command=PerfilCaptacao");
					} else {
						response.sendRedirect("Controller?command=PerfilAnalista");
					}
				} else {
					response.sendRedirect("Login.jsp?command=null");
				}
			}
		} catch(IOException | SQLException e) {
			e.printStackTrace();
		}
	}
}
