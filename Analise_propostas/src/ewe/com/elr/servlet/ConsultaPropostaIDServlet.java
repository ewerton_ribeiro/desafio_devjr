package ewe.com.elr.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ewe.com.elr.Beans.PropostaJB;
import ewe.com.elr.DAO.PropostaDAO;

public class ConsultaPropostaIDServlet extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PropostaDAO DAO = new PropostaDAO();
		PrintWriter out = response.getWriter();
		String idString = request.getParameter("ID");
		
		PropostaJB proposta = new PropostaJB();
		
		try {
			proposta = DAO.consultaProposta(idString);
			
			
			out.println("<html>");
			out.println("<body style = 'background-color: lightblue'>");
			out.println("<h1>" +"Proposta"+ "</h1>");		
			out.println("<style 'text/css>" + ".button-SignOf {position: absolute; top: 15px;right: 20px;}" + "</style>");
			out.println("<a class='button-SignOf'href='Controller?command=Deslogar'><button>SignOf</button></a>");								
				
			out.println("ID: " + proposta.getID() + "<br/>");
			out.println("Nome: " + proposta.getNome() + "<br/>"); 
			out.println("CPF do Cliente: " + proposta.getCpf() + "<br/>");
			out.println("Salário: " + proposta.getSalario() + "<br/>");
			out.println("Status da Proposta: " + proposta.getStatus() + "<br/>");
			
			out.println("<a href='Controller?command=AnaliseDeCredito'><button>Realizar Analise de Crédito</button></a>");

			
			out.println("</body>");
			out.println("</html>");
			
		} catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		

	}

}
