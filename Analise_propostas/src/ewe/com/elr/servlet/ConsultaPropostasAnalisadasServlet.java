package ewe.com.elr.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ewe.com.elr.Beans.PropostaJB;
import ewe.com.elr.DAO.PropostaDAO;

//@WebServlet("/ConsultaPropostasAnalisadas")
public class ConsultaPropostasAnalisadasServlet extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PropostaDAO DAO = new PropostaDAO();
		PrintWriter out = response.getWriter();
		List<PropostaJB> propostas = new ArrayList<>();
		
		try {
			propostas = DAO.consultaPropostasAnalisadas();
			
			out.println("<html>");
			out.println("<body style = 'background-color: lightblue'>");
			out.println("<h1>" +"Propostas Analisadas"+ "</h1>");
	
			
			out.println("<a href='Controller?command=CaptacaoProposta'><button>Cadastro de Proposta</button></a><br/>");
			
			out.println("<style 'text/css>" + ".button-SignOf {position: absolute; top: 15px;right: 20px;}" + "</style>");
			out.println("<a class='button-SignOf' href='Controller?command=Deslogar'><button>SignOf</button></a><br/>");
			
			for (PropostaJB propostaJB : propostas) {
								
				out.println("<ul>");
				out.println("<li>" + "ID da Proposta: "+ propostaJB.getID() + "</li>");
				out.println("<li>" + "Nome do Cliente: "+ propostaJB.getNome() + "</li>"); 
				out.println("<li>" + "CPF: "+ propostaJB.getCpf() + "</li>");
				out.println("<li>" + "Status da Proposta: "+ propostaJB.getStatus() + "</li>");
				out.println("</ul>");			
				
			}
			
			out.println("</body>");
			out.println("</html>");
			
		} catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		

	}

}
