package ewe.com.elr.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;

import ewe.com.elr.Beans.PropostaJB;
import ewe.com.elr.DAO.PropostaDAO;

public class CadastroPropostaServlet extends HttpServlet{

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		
		String nome = request.getParameter("nome");
		String salarioString = request.getParameter("salario");
		String cpf = request.getParameter("cpf");
		
		String statusProposta = "pendente";
		
		Double salario = Double.parseDouble(salarioString);
		
		PropostaJB Proposta = new PropostaJB();
		
		Proposta.setCpf(cpf);
		Proposta.setNome(nome);
		Proposta.setSalario(salario);
		Proposta.setStatus(statusProposta);
		
		PropostaDAO DAO = new PropostaDAO();
		try {
			DAO.cadastraProposta(Proposta);
		} catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		out.println("<html>");
		out.println("<body style = 'background-color: lightblue'>");
		
		out.println("A proposta do cliente " + Proposta.getNome() + " foi cadastrada com sucesso!");
		out.println("<br/>");
		out.println("<a href='Controller?command=CaptacaoProposta'>Voltar a tela de cadastro de propostas</a><br/>");
		out.println("<a href='Controller?command=Deslogar'><button>SignOf</button></a><br/>");
		out.println("<body>");
		out.println("<html>");
	}
}
