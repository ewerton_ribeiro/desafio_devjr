package ewe.com.elr.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ewe.com.elr.Beans.PropostaJB;
import ewe.com.elr.DAO.PropostaDAO;

// Alterando status da proposta analisada
public class AlteraStatusPropostaServlet extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		
		String status = request.getParameter("status");
		String idString = request.getParameter("id");
		
		int id = Integer.parseInt(idString);
		
		PropostaDAO DAO = new PropostaDAO();
		
		try {
			DAO.alteraStatusProposta(status, id);
		} catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		out.println("<html>");
		out.println("<body>");
		out.println("A proposta de ID " + id + " foi atualizada com sucesso!");
		out.println("<br/>");
		out.println("<br/>");
		out.println("<a href='Controller?command=AnaliseDeCredito'>Voltar a tela de propostas</a>");
		out.println("</body>");
		out.println("</html>");
	}
}
