package ewe.com.elr.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import ewe.com.elr.Beans.PropostaJB;
import ewe.com.elr.DAO.PropostaDAO;

public class ConsultaPropostaPendenteServlet extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PropostaDAO DAO = new PropostaDAO();
		PrintWriter out = response.getWriter();
		
		List<PropostaJB> propostas = new ArrayList<>();
		
		try {
			propostas = DAO.consultaPropostasPendentes();
			out.println("<html>");
			out.println("<body style = 'background-color: lightblue'>");
			out.println("<h1>" +"Proposta Pendentes"+ "</h1>");
			
			out.println("<a href='Controller?command=AnaliseDeCredito'><button>Realizar Analise de Crédito</button></a>");
			
			out.println("<style 'text/css>" + ".button-SignOf {position: absolute; top: 15px;right: 20px;}" + "</style>");
			out.println("<a class='button-SignOf'href='Controller?command=Deslogar'><button>SignOf</button></a><br/>");
			
			for (PropostaJB propostaJB : propostas) {
								
				out.println("<ul>");
				out.println("<li>" + "ID da Proposta: " + propostaJB.getID() + "</li>");
				out.println("<li>" + "Nome do Cliente: " + propostaJB.getNome() + "</li>"); 
				out.println("<li>" + "CPF: " + propostaJB.getCpf() + "</li>");
				out.println("<li>" + "Salario: "+ propostaJB.getSalario() + "</li>");
				out.println("<li>" + "Status da Proposta: "+ propostaJB.getStatus() + "</li>");
				out.println("</ul>");
				out.println();
				
			}
			
			out.println("</body>");
			out.println("</html>");
			
		} catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		

	}
}

