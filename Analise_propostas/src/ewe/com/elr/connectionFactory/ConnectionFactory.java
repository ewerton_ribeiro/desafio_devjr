package ewe.com.elr.connectionFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	    
	    public static Connection getConnection() throws SQLException{
	    	
	    	
	        try{
	        	
	        	Class.forName("com.mysql.jdbc.Driver");
	        	
	        } catch(ClassNotFoundException e){
	        	
	            throw new SQLException(e);
	        }
	        
	        return DriverManager.getConnection("jdbc:mysql://localhost:3306/db_analise_credito", "root", "veronica2412");


	    }
	    

}

