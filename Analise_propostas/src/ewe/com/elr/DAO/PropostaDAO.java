package ewe.com.elr.DAO;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ewe.com.elr.Beans.PropostaJB;
import ewe.com.elr.connectionFactory.ConnectionFactory;

public class PropostaDAO {
	
	private Connection connection;
	private ResultSet rs;
	public PropostaDAO() {
		
		try {
			new ConnectionFactory();
			this.connection = ConnectionFactory.getConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void cadastraProposta(PropostaJB proposta) throws SQLException, ClassNotFoundException {
	
		try {
		
			PreparedStatement stmt = this.connection.prepareStatement("INSERT INTO proposta" + "(Nome_Cliente, CPF_Cliente, Salario, Status_proposta)" + "values (?,?,?,?)");
		
			stmt.setString(1, proposta.getNome());
			stmt.setString(2, proposta.getCpf());
			stmt.setDouble(3, proposta.getSalario());
			stmt.setString(4, proposta.getStatus());
			
			stmt.execute();
			stmt.close();
		} catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public void alteraStatusProposta(String status, int id) throws SQLException, ClassNotFoundException {
		
		try {
			
			PreparedStatement stmt = this.connection.prepareStatement("UPDATE proposta SET Status_proposta=? WHERE ID_Proposta = ?");
		
			stmt.setString(1, status);
			stmt.setInt(2, id);
			
			stmt.execute();
			stmt.close();
		} catch(SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<PropostaJB> consultaPropostasPendentes() throws SQLException, ClassNotFoundException {
		
		List<PropostaJB> propostas = new ArrayList<>();
		
			try {
				
				PreparedStatement stmt = this.connection.prepareStatement("Select * From proposta WHERE Status_proposta ='pendente'");			
				rs = stmt.executeQuery();
				
				while(rs.next()) {
					
					PropostaJB prop = new PropostaJB();
					prop.setID(rs.getInt("ID_Proposta"));
					prop.setNome(rs.getString("Nome_Cliente"));
					prop.setCpf(rs.getString("CPF_Cliente"));
					prop.setSalario(rs.getDouble("Salario"));
					prop.setStatus(rs.getString("Status_proposta"));
					
					
					
					propostas.add(prop);
				}
				stmt.close();
			} catch(SQLException e) {
				throw new RuntimeException(e);
									
			}
		
		return propostas;
	}
	
public PropostaJB consultaProposta(String id) throws SQLException, ClassNotFoundException {
	
	int idProposta = Integer.parseInt(id);
	
	PropostaJB prop = new PropostaJB();
		
			try {
				
				PreparedStatement stmt = this.connection.prepareStatement("Select * From proposta WHERE ID_Proposta =?");
				
				stmt.setInt(1, idProposta);
				
				rs = stmt.executeQuery();
				
				while(rs.next()) {
					
					prop.setID(rs.getInt("ID_Proposta"));
					prop.setNome(rs.getString("Nome_Cliente"));
					prop.setCpf(rs.getString("CPF_Cliente"));
					prop.setSalario(rs.getDouble("Salario"));
					prop.setStatus(rs.getString("Status_proposta"));
					
				}
				stmt.close();
			} catch(SQLException e) {
				throw new RuntimeException(e);
									
			}
		
		return prop;
	}

public List<PropostaJB> consultaPropostasAnalisadas() throws SQLException, ClassNotFoundException {
	
	List<PropostaJB> propostas = new ArrayList<>();
	
		try {
			
			PreparedStatement stmt = this.connection.prepareStatement("Select * From proposta WHERE Status_proposta !='pendente'");			
			rs = stmt.executeQuery();
			
			
			while(rs.next()) {
				
				PropostaJB prop = new PropostaJB();
				prop.setID(rs.getInt("ID_Proposta"));
				prop.setNome(rs.getString("Nome_Cliente"));
				prop.setCpf(rs.getString("CPF_Cliente"));
				prop.setSalario(rs.getDouble("Salario"));
				prop.setStatus(rs.getString("Status_proposta"));
				
				
				
				propostas.add(prop);
			}
			stmt.close();
		} catch(SQLException e) {
			throw new RuntimeException(e);
								
		}
	
	return propostas;
}

public PropostaJB consultaPropostaAnalisada(String idString)  throws SQLException, ClassNotFoundException {
	
	int idProposta = Integer.parseInt(idString);
	
	PropostaJB prop = new PropostaJB();
		
			try {
				
				PreparedStatement stmt = this.connection.prepareStatement("Select * From proposta WHERE ID_Proposta =? and Status_proposta !='pendente'");
				
				stmt.setInt(1, idProposta);
				
				rs = stmt.executeQuery();
				
				while(rs.next()) {
					
					prop.setID(rs.getInt("ID_Proposta"));
					prop.setNome(rs.getString("Nome_Cliente"));
					prop.setCpf(rs.getString("CPF_Cliente"));
					prop.setSalario(rs.getDouble("Salario"));
					prop.setStatus(rs.getString("Status_proposta"));
					
				}
				stmt.close();
			} catch(SQLException e) {
				throw new RuntimeException(e);
									
			}
		
		return prop;
	
}
}
