package ewe.com.elr.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ewe.com.elr.connectionFactory.ConnectionFactory;


public class UsuarioDAO {
	
	
	private static PreparedStatement stmt = null;
	private static ResultSet rs = null;
	
	public static boolean checkLogin(String login, String senha) throws SQLException{
		Connection con = ConnectionFactory.getConnection();
		
		boolean check = false;
		
		try {
			stmt = con.prepareStatement("SELECT * FROM usuario WHERE Login_usuario = ? AND senha_login = ?");
			stmt.setString(1, login);
			stmt.setString(2, senha);
			
			rs = stmt.executeQuery();
			
			if(rs.next()) {
				check = true;
			}
		} catch(SQLException e) {
			Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
		}
		return check;
	}
	
public static String checkTipo(String login) throws SQLException {
	
	Connection con = ConnectionFactory.getConnection();
		
		
		String tipo = "";
		
		try {
			stmt = con.prepareStatement("SELECT Tipo_Usuario FROM usuario WHERE Login_usuario = ?");
			stmt.setString(1, login);
			
			rs = stmt.executeQuery();
			
			if(rs.next()) {
				tipo = rs.getString("Tipo_Usuario");
			}
			
		} catch(SQLException e) {
			throw new RuntimeException(e);
		}
		return tipo;
		
	}
}

