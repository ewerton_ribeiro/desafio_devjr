package ewe.com.elr.Beans;

public class UsuarioJB {
    private String Nome;
    private String cpfUsuario;
    private String Login;
    private String Senha;
    private String Tipo;
    
    
    public String getNome(){
        return this.Nome;
    }
    
    public void setNome(String novoNome){
        this.Nome = novoNome;
    }
    
    public String getCpf() {
		return this.cpfUsuario;
	}

	public void setCpf(String novoCpf) {
		this.cpfUsuario = novoCpf;
	}
	
	public String getLogin() {
		return this.Login;
	}

	public void setLogin(String novoLogin) {
		this.Login = novoLogin;
	}

	public String getSenha() {
		return this.Senha;
	}

	public void setSenha(String novaSenha) {
		this.Senha = novaSenha;
	}

	public String getTipo() {
		return this.Tipo;
	}

	public void setTipo(String novoTipo) {
		this.Tipo = novoTipo;
	}

}
