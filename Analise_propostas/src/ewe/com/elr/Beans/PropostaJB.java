package ewe.com.elr.Beans;

public class PropostaJB {
	
	private int ID;
	private String cpfCliente;
	private String Nome;
	private double Salario;
	private String statusProposta;
	
	public String getCpf(){
		return this.cpfCliente;
	}
	
	public void setCpf(String novoCpf) {
		this.cpfCliente = novoCpf;
	}

	public String getNome() {
		return this.Nome;
	}

	public void setNome(String novoNome) {
		this.Nome = novoNome;
	}

	public double getSalario() {
		return this.Salario;
	}

	public void setSalario(double novoSalario) {
		this.Salario = novoSalario;
	}


	public String getStatus() {
		return this.statusProposta;
	}

	public void setStatus(String novoStatus) {
		this.statusProposta = novoStatus;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
}
