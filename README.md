# Desafio_DevJr

# Sistema de cadastro de proposta 

# Tecnologias utilizadas no desenvolvimento. 

- IDE: Eclipse
- Linguagem back-end: Java, xml.
- Linguagem Front-end: HTML, CSS, JavaScript
- 
- Servidor web: TomCat
- Framework: Sevlets
- Banco de dados: MySQL 

Requisitos para utilizar 

- TomCat versão 9 ou superior.
- Banco de dados MySQL
    Será necessario alterar a url do banco na classe Connectionfactory assim como o usuario e senha.
- Ambiente de desenvolvimento Java (JEE/JDK 1.8)
- Também será necessário criar a base de dados. A qual o arquivo de criação já foi disponibilizado na pasta raiz do projeto (createDatabase.sql);

Url de acesso ao sistema: http://localhost:8080/Analise_propostas/Login.jsp?command=null

# Guia de utilização Usuário 

- Os usuários são dividos em 2 dos tipos diferentes usuários, captação e analista que serão diferenciados no momento do login para terem acesso a workpages diferentes, onde estão às tarefas do seu escopo. 

O arquivo de criação já insere os 2 tipos de usuarios na base.
usuario captacao: gustavologin senha: 12345
usuario analista: ewertonlogin senha: 12345

# Recomendacões de uso usuário Captação

Cadastrando uma proposta:
Para isso basta clicar no botão da tarefa e preencher os dados do cliente, depois clicar em cadastrar proposta, após isso aparecerá uma mensagem na tela confirmando o cadastro, você poderá utilizar o link de acesso para continuar cadastrando propostas. caso queria, também poderá consultar as propostas analisadas na tela  de cadastro. 

Consultando propostas analisadas: 
Basta clicar no botão para obter os dados das propostadas que já foram analisadas, e estão com status aprovada ou recusada.

Consultando proposta por ID:
Basta digitar o Id da proposta desejada e clicar no botão para obter o resultado com o ID desejado.


# Recomendções usuario analista 

Consultando propostas analisadas: 
Basta clicar no botão para obter os dados das propostadas que ainda não foram analisadas, e estão com status pendentes.

Consultando proposta por ID:
Basta digitar o Id da proposta desejada e clicar no botão para obter o resultado com o ID desejado.

Realizando analise de crédito: 
Ao acessar a tarefa basta digitar o ID da proposta que deseja inserir a analise, e digitar o status da propota após analisada (aprovada ou recusada), após isso basta clicar em atualizar proposta para salvar a analise. também é possivel consultar as propostas pendentes de analise a partir deste ponto.






