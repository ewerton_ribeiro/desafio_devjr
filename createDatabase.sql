create database db_analise_credito;

use db_analise_credito;

create table proposta (

ID_Proposta int(10) NOT NULL AUTO_INCREMENT,
Nome_Cliente varchar(45) NOT NULL,
CPF_Cliente varchar(11) NOT NULL,
Salario decimal(10,2) NOT NULL,
Status_proposta varchar(10) NOT NULL,

PRIMARY KEY (ID_Proposta)


); 

LOCK TABLES `proposta` WRITE;
UNLOCK TABLES;

create table usuario (

Nome_usuario varchar (45) NOT NULL,
CPF_usuario   varchar (15) NOT NULL,
Login_usuario varchar (50) NOT NULL,
senha_login 	varchar(45) NOT NULL,
Tipo_Usuario  varchar(45) NOT NULL


);

LOCK TABLES `usuario` WRITE;
UNLOCK TABLES;

insert into usuario (Nome_usuario, CPF_usuario, Login_usuario, senha_login, Tipo_Usuario) 
values ('Ewerton Ribeiro', ' 433.667.985-90' , 'ewertonlogin', '12345' , 'analista');

insert into usuario (Nome_usuario, CPF_usuario, Login_usuario, senha_login, Tipo_Usuario) 
values ('Gustavo Ribeiro', '433.667.885-95' , 'gustavologin', '12345' , 'captacao');